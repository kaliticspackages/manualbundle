<?php

namespace Kalitics\ManualBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kalitics\ManualBundle\Entity\UserDocumentation\Blog;

/**
 * ReferencedRoute
 *
 * @ORM\Table(name="user_documentation_referenced_route")
 * @ORM\Entity(repositoryClass="Kalitics\ManualBundle\Repository\ReferencedRouteRepository")
 */
class ReferencedRoute
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=255, unique=true)
     */
    private $route;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var Blog
     * @ORM\ManyToOne(targetEntity="Kalitics\ManualBundle\Entity\UserDocumentation\Blog", inversedBy="referencedRoutes", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", name="blog", nullable=false)
     */
    private $blog;

    /**
     * ReferencedRoute constructor.
     * @param string $route
     */
    public function __construct( string $route )
    {
        $this->route = $route;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set route.
     *
     * @param string $route
     *
     * @return ReferencedRoute
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route.
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return ReferencedRoute
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return null|Blog
     */
    public function getBlog(): ?Blog
    {
        return $this->blog;
    }

    /**
     * @param Blog $blog
     */
    public function setBlog(Blog $blog): void
    {
        $this->blog = $blog;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        if( $this->label !== null ){
            return $this->label;
        }
        return $this->route;
    }
}
