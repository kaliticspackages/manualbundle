<?php


namespace Kalitics\ManualBundle\Entity\UserDocumentation;

use ContextManager\Entity\ContextualizedEntity;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractContentElement extends ContextualizedEntity  implements IContentElement {

    /**
     * @var int
     * @ORM\Column( name="width", type="integer", length=2 )
     */
    protected $width;

    /**
     * @var int
     * @ORM\Column( name="blog_position", type="integer")
     */
    protected $position;

    /**
     * @var Blog
     * @ORM\ManyToOne(targetEntity="Kalitics\ManualBundle\Entity\UserDocumentation\Blog")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $blog;

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return "";
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @return Blog
     */
    public function getBlog(): Blog
    {
        return $this->blog;
    }

    /**
     * @param Blog $blog
     */
    public function setBlog(Blog $blog): void
    {
        $this->blog = $blog;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }


    /**
     * @return string
     */
    public function renderView(): string
    {
        return  sprintf("<div class='col-sm-%s' data-class='%s' data-id='%s'>%s</div>",$this->getWidth(), $this->getElementType(), $this->getId(), $this->getContent());
    }

    public function getElementType(): string
    {
        $class = explode("\\",get_class($this));
        return end($class);
    }

}