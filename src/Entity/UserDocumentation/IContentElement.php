<?php

namespace Kalitics\ManualBundle\Entity\UserDocumentation;

/**
 * Interface IContentElement
 * @package Manual\Entity\UserDocumentation
 */
interface IContentElement{

    /**
     * @return int
     */
    public function getWidth():int;

    /**
     * @return string
     */
    public function getContent():string;

    /**
     * @param array $data
     * @return mixed
     */
    public function setContent( Array $data);

    /**
     * @return int
     */
    public function getPosition():int;

    /**
     * @return mixed
     */
    public function setPosition(int $position);

    /**
     * @return string
     */
    public function renderView():string;

    /**
     * @param Blog $blog
     * @return mixed
     */
    public function setBlog(Blog $blog);

    /**
     * @return Blog
     */
    public function getBlog():Blog;


}