<?php

namespace Kalitics\ManualBundle\Entity\UserDocumentation;


use ContextManager\Entity\ContextualizedEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Kalitics\ManualBundle\Entity\ReferencedRoute;
use Kalitics\ManualBundle\Entity\UserDocumentation\ContentElement\CkeditorElement;

/**
 * Blog
 *
 * @ORM\Table(name="user_documentation_blog")
 * @ORM\Entity(repositoryClass="Kalitics\ManualBundle\Repository\UserDocumentation\BlogRepository")
 */
class Blog extends ContextualizedEntity
{
    /**
     * @var string|null
     *
     * @ORM\Column(name="todoList", type="text", nullable=true)
     */
    private $todoList;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Kalitics\ManualBundle\Entity\ReferencedRoute", mappedBy="blog", cascade={"remove", "persist"})
     */
    private $referencedRoutes;


    public function __construct()
    {
        parent::__construct();
        $this->referencedRoutes = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set todoList.
     *
     * @param string|null $todoList
     *
     * @return Blog
     */
    public function setTodoList($todoList = null): Blog
    {
        $this->todoList = $todoList;

        return $this;
    }

    /**
     * Get todoList.
     *
     * @return string|null
     */
    public function getTodoList(): ?string
    {
        return $this->todoList;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return Blog
     */
    public function setTitle($title = null): Blog
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @return Collection
     */
    public function getReferencedRoutes()
    {
        return $this->referencedRoutes;
    }

    /**
     * @param Collection $referencedRoutes
     */
    public function setReferencedRoutes($referencedRoutes): void
    {
        $this->referencedRoutes = $referencedRoutes;
    }

    /**
     * @param ReferencedRoute $referencedRoute
     */
    public function addReferencedRoutes( ReferencedRoute $referencedRoute ): void
    {
        $this->referencedRoutes->add($referencedRoute);
        $referencedRoute->setBlog($this);
    }

    public function addReferencedRoute(ReferencedRoute $referencedRoute): self
    {
        if (!$this->referencedRoutes->contains($referencedRoute)) {
            $this->referencedRoutes[] = $referencedRoute;
            $referencedRoute->setBlog($this);
        }

        return $this;
    }

    public function removeReferencedRoute(ReferencedRoute $referencedRoute): self
    {
        if ($this->referencedRoutes->removeElement($referencedRoute)) {
            // set the owning side to null (unless already changed)
            if ($referencedRoute->getBlog() === $this) {
                $referencedRoute->setBlog(null);
            }
        }

        return $this;
    }

}
