<?php

namespace Kalitics\ManualBundle\Entity\UserDocumentation\ContentElement;

use Kalitics\ManualBundle\Entity\UserDocumentation\AbstractContentElement;
use Doctrine\ORM\Mapping as ORM;


/**
 * ImageElement
 *
 * @ORM\Table(name="user_documentation_image_element")
 * @ORM\Entity(repositoryClass="Kalitics\ManualBundle\Repository\UserDocumentation\ContentElement\ImageElementRepository")
 */
class ImageElement extends AbstractContentElement
{
    /**
     * @var string
     *
     * @ORM\Column(name="pictureContent", type="string", length=255)
     */
    private $pictureContent;

    /**
     * @var string
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;


    /**
     * Get id.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set pictureContent.
     *
     * @param string $pictureContent
     *
     * @return ImageElement
     */
    public function setPictureContent(string $pictureContent): ImageElement
    {
        $this->pictureContent = $pictureContent;

        return $this;
    }

    /**
     * Get pictureContent.
     *
     * @return string
     */
    public function getPictureContent(): string
    {
        return $this->pictureContent;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }



    public function getContent(): string
    {
        return sprintf("<img src='%s/%s' alt='%s' class='manual-picture' >",$this->getPath(), $this->getPictureContent(), $this->getPictureContent());
    }

    public function setContent(array $data)
    {
        if( isset($data['files']['image']['filename']) && isset($data['files']['image']['path']) && isset($data['values']['element-width'])){
            $this->setWidth($data['values']['element-width']);
            $this->setPictureContent( $data['files']['image']['filename']);
            $this->setPath( $data['files']['image']['path']);
        }
    }


}
