<?php

namespace Kalitics\ManualBundle\Entity\UserDocumentation\ContentElement;

use Kalitics\ManualBundle\Entity\UserDocumentation\AbstractContentElement;
use Doctrine\ORM\Mapping as ORM;

/**
 * CkeditorElement
 *
 * @ORM\Table(name="user_documentation_ckeditor_element")
 * @ORM\Entity(repositoryClass="Kalitics\ManualBundle\Repository\UserDocumentation\ContentElement\CkeditorElementRepository")
 */
class CkeditorElement extends AbstractContentElement
{

    /**
     * @var string
     *
     * @ORM\Column(name="ckeditorContent", type="text")
     */
    private $ckeditorContent;


    /**
     * Get id.
     *
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Set ckeditorContent.
     *
     * @param string $ckeditorContent
     *
     * @return CkeditorElement
     */
    public function setCkeditorContent(string $ckeditorContent): CkeditorElement
    {
        $this->ckeditorContent = $ckeditorContent;

        return $this;
    }

    /**
     * Get ckeditorContent.
     *
     * @return string
     */
    public function getCkeditorContent(): string
    {
        return $this->ckeditorContent;
    }

    public function getContent(): string
    {
        return $this->getCkeditorContent();
    }

    public function setContent(array $data)
    {
        if( isset($data['values']['ckeditor-content']) && isset($data['values']['element-width'])){
            $this->setWidth($data['values']['element-width']);
            $this->setCkeditorContent( $data['values']['ckeditor-content']);
        }
    }

}
