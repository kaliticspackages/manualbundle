<?php

namespace Kalitics\ManualBundle;

use Kalitics\ManualBundle\DependencyInjection\KaliticsManualExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KaliticsManualBundle extends Bundle
{

    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new KaliticsManualExtension();
        }
        return $this->extension;
    }
}
