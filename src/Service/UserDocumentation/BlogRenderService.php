<?php

namespace Kalitics\ManualBundle\Service\UserDocumentation;

use Kalitics\ManualBundle\Entity\UserDocumentation\Blog;
use Kalitics\ManualBundle\Manager\BlogManager;
use Symfony\Component\Yaml\Parser;

/**
 * Class BlogRenderService
 * @package ManualBundle\Service\UserDocumentation
 */
class BlogRenderService{

    const DATA = '../../Data/ContentElementResource.yml';

    /**
     * @var BlogManager
     */
    private $blogManager;

    /**
     * @var Blog
     */
    private $blog;

    /**
     * BlogRenderService constructor.
     * @param BlogManager $blogManager
     */
    public function __construct(
        BlogManager $blogManager
    )
    {
        $this->blogManager = $blogManager;
    }

    /**
     * @param $blog|int
     * @return $this
     */
    public function createBlogRender( $blog ): BlogRenderService
    {
        if( $blog instanceof Blog ){
            $this->blog = $blog;
        }else{
            $this->blog = $this->blogManager->getBlog($blog);
        }
        return $this;
    }

    /**
     * @param bool $showEditBtn
     * @return string
     */
    public function renderView($showEditBtn = true):string
    {
        $editBtn = $showEditBtn ? <<<HTML
            <button class='btn btn-label btn-label-primary btn-bold btn-upper' data-toggle='tooltip' title='Modifier le titre' id='btn-rename'>
                <i class='fas fa-pen'></i>
            </button>
        HTML : '';

        $render = "";
        foreach ( $this->blogManager->getSortedBlogElements( $this->blog ) as $contentElement ){
            $render .= $contentElement->renderView();
        }

        $html = <<<HTML
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon"><i class="kt-font-primary fas fa-book-open"></i></span>
                        <h3 class="kt-portlet__head-title">%s</h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            <div class="kt-portlet__head-actions">
                                {$editBtn}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div style='display: flex;flex-direction: column;align-items: center;' class="row blog-main-render">
                       %s
                    </div>
                </div>
            </div>
        HTML;

        return  sprintf($html, $this->blog->getTitle(), $render);
    }


    /**
     * @return string
     * Renvoie les boutons de sélection
     */
    public function renderSelectButtons(): string
    {
        $buttons = "";
        foreach ( BlogManager::CONTENT_ELEMENTS as $class => $attributes ){
            $buttons .=  sprintf("<button class='btn btn-success' data-class='%s'>%s <br> %s </button>",
                $this->blogManager->getTypeFromClass($class),
                $attributes['icon'],
                $attributes['label']);
        }
        return $buttons;
    }

    /**
     * @param string $type
     * @return string
     */
    public function getTemplateContentElement( string $type ): string
    {
        $paths = (new Parser)->parseFile(__DIR__."/".(self::DATA));
        return $paths[$type];
    }

    /**
     * @return BlogManager
     */
    public function getBlogManager(): BlogManager
    {
        return $this->blogManager;
    }



}