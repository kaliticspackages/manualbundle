<?php

namespace Kalitics\ManualBundle\Controller;


use Kalitics\ManualBundle\Entity\UserDocumentation\IContentElement;
use Kalitics\ManualBundle\Form\ReferencedRouteType;
use Kalitics\ManualBundle\Form\UserDocumentation\BlogType;
use Kalitics\ManualBundle\Manager\BlogManager;
use Kalitics\ManualBundle\Service\UserDocumentation\BlogRenderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;


class UserDocumentationController extends AbstractController
{
    /**
     * @var BlogManager
     */
    private $blogManager;
    /**
     * @var BlogRenderService
     */
    private $blogRenderService;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * UserDocumentationController constructor.
     * @param BlogManager $blogManager
     * @param BlogRenderService $blogRenderService
     * @param RouterInterface $router
     */
    public function __construct(
        BlogManager $blogManager,
        BlogRenderService $blogRenderService,
        RouterInterface $router
    )
    {
        $this->blogManager = $blogManager;
        $this->blogRenderService = $blogRenderService;
        $this->router = $router;
    }


    public function blogListAction(): Response
    {
        return $this->render("@KaliticsManual/UserDocumentation/blog_list.html.twig", [
           "blogs"      => $this->blogManager->findAll()
        ]);
    }

    public function renderBlogAction( $id ): ?Response
    {
        return $this->render("@KaliticsManual/UserDocumentation/render_blog_view.html.twig", [
            "blog"  => $this->blogRenderService->createBlogRender($id),
            'currentId' => $id,
            'blogs' => $this->blogManager->findAll()
        ]);
    }

    public function renderBlogEditionAction( $id ): ?Response
    {
        return $this->render("@KaliticsManual/UserDocumentation/render_blog.html.twig", [
            "blog"      => $this->blogRenderService->createBlogRender($id),
            'currentId' => $id,
            'blogs' => $this->blogManager->findAll()
        ]);
    }

    public function deleteBlogAction( $id ): ?Response
    {
        $blog = $this->blogManager->getBlog($id);
        $this->blogManager->deleteBlog($blog);

        return $this->redirectToRoute('user_documentation_list');
    }

    public function getBlogRenderAction( $id ): ?Response
    {
        $blogRender = $this->blogRenderService->createBlogRender($id);
        return new JsonResponse([
            "render"    => $blogRender->renderView()
        ]);
    }

    public function editBlogAction(Request $request, $path): Response
    {
        $blog = $this->blogManager->getBlogFromPath( $path );
        if( null === $blog ){
            $blog = $this->blogManager->createBlog( $path );
        }
        $form = $this->createForm(BlogType::class, $blog, [
            "action"    => $this->generateUrl($request->get('_route'), ["path" => $path])
        ]);
        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid()){
            $this->blogManager->save($blog);
            return $this->redirectToRoute("user_documentation_render_edition", [ "id" => $blog->getId()]);
        }
        return $this->render("@KaliticsManual/UserDocumentation/modal/add_documentation.html.twig", [
            "form"      => $form->createView()
        ]);
    }


    public function attachBlogForPathAction(Request $request, $path): Response
    {
        $refRoute = $this->blogManager->getReferencedRouteFromPath( $path );

        $form = $this->createForm(ReferencedRouteType::class, $refRoute, [
            "action"    => $this->generateUrl($request->get('_route'), ['path' => $path])
        ]);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() ){
           $this->blogManager->saveReferencedRoute( $refRoute );
            return new JsonResponse([
                "id"        => $refRoute->getBlog()->getId(),
            ]);
        }
        return $this->render("@KaliticsManual/UserDocumentation/modal/attach_documentation.html.twig", [
            'form'  => $form->createView()
        ]);

    }

    public function unlockBlogFromPathAction( $id ): JsonResponse
    {
        $refRoute = $this->blogManager->getReferencedRoute( $id );
        $blog = $refRoute->getBlog();
        $this->blogManager->deleteReferencedRoute($refRoute);
        return new JsonResponse([
            "id"    => $id,
        ]);
    }

    public function renameBlogAction( Request $request, $id ): ?Response
    {
        $blog = $this->blogManager->getBlog($id);
        $form = $this->createForm(BlogType::class, $blog, [
            "action"        => $this->generateUrl("user_documentation_rename", [ 'id' => $id ])
        ]);
        $form->handleRequest( $request );
        if( $form->isSubmitted() && $form->isValid() ){
            $this->blogManager->save($blog);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    "id"        => $id,
                    "name"      => $blog->getTitle()
                ]);
            }

            return $this->redirectToRoute("user_documentation_list");
        }
        return $this->render("@KaliticsManual/UserDocumentation/modal/add_documentation.html.twig", [
            "form"      => $form->createView(),
            "blog"      => $blog
        ]);
    }


    public function addContentElementAction( $id, $type ): ?Response
    {
        $template = $this->blogRenderService->getTemplateContentElement( $type );
        return $this->render($template, [
            "type"  => $type,
            "blog"  => $this->blogManager->getBlog($id),
            "action"=> $this->generateUrl("user_documentation_create_content", ["id" => $id, "type" => $type])
        ]);
    }


    public function updateContentElementAction(Request $request, $id, $type ): ?Response
    {
        $contentElement = $this->blogManager->getContentElement( $type, $id );
        $template = $this->blogRenderService->getTemplateContentElement( $type );

        return $this->render($template, [
            "type"      => $type,
            'width'     => $contentElement->getWidth(),
            "blog"      => $contentElement->getBlog(),
            "content"   => $contentElement->getContent(),
            "action"=> $this->generateUrl("user_documentation_save_content", ["id" => $id,  "type" => $type])
        ]);
    }


    public function createAndSaveContentElementAction( Request $request, $id, $type):JsonResponse
    {
        $blog = $this->blogManager->getBlog($id);
        $content = $this->blogManager->createContentFromRequestData( $blog, $type, $request );
        return new JsonResponse([
            "id"        => $content->getId(),
            "content"   => $content->renderView()
        ]);
    }


    public function saveContentElementAction( Request $request, $id, $type):JsonResponse
    {
        $contentElement = $this->blogManager->getContentElement( $type, $id );
        $newContent     = $this->blogManager->updateContentFromRequestData( $contentElement, $type, $request );

        return new JsonResponse([
            "id"        => $contentElement->getId(),
            "content"   => $contentElement->renderView()
        ]);
    }


    public function getHeaderbarMenuIconsAction( Request $request, $path, $display ): ?Response
    {
        return $this->render("@KaliticsManual/UserDocumentation/header_buttons.html.twig", [
            "blog"  => $this->blogManager->getBlogFromPath($path),
            "route" => $path,
            'display' => $display
        ]);
    }


    public function sortContentElementsAction( Request $request, $id): ?Response
    {
        $data =  json_decode($request->getContent(), true);
        $blog = $this->blogManager->getBlog($id);
        $this->blogManager->sortElementsFromRequestData( $blog, $data);
        return new JsonResponse();
    }

    public function resizeContentElementAction( Request $request, $id, $type ): ?Response
    {
        /** @var ?IContentElement $contentElement */
        $contentElement = $this->blogManager->getContentElement( $type, $id );

        if( $request->request->get('size') !== null ){
            if( $contentElement !== null ){
                $this->blogManager->resizeElement( $contentElement, $request->request->get('size')  );
            }
            return new JsonResponse('ok');
        }
        return $this->render("@KaliticsManual/UserDocumentation/modal/resize.html.twig", [
            "contentElement" => $contentElement,
            "type"           => $type
        ]);
    }

    public function removeContentElementAction( Request $request, $id, $type ): ?Response
    {
        /** @var ?IContentElement $contentElement */
        $contentElement = $this->blogManager->getContentElement( $type, $id );
        if( $contentElement !== null ){
            $this->blogManager->deleteElement($contentElement);
            return new JsonResponse();
        }
        return new JsonResponse([], Response::HTTP_NOT_FOUND);
    }

}
