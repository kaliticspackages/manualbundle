function renameBlog( id ){
    var url = ($("#url-rename-blog").val()).replace('ID', id);
    openModal('modal', url)
}

function submitRename(){
    postFormAjax( "#form-update-blog", data => {
        if( data.id != undefined ){
            $("#tr-blog-"+data.id+" .blog-name").text(data.name);
            $("#modal").modal('hide');
        }
    } )
}

function unlinkPath ( element, url ){
    getJsonFromController( url , data => {
        $(element).parent().hide('fast');
    } )

}

function  postFormAjax(formId, successCallback) {
    $(formId).append(`
    <div class="row ajax-loader m-t-20">
        <div class="col-sm-12 text-center">
           <!-- mettre spinner -->
        </div>
    </div>
    `)
    return submitFormAjaxAndDoAction($(formId)[0].action, formId, successCallback);
}

function getJsonFromController(path, callback){
    submitFormAjaxAndDoAction(path, null, callback);
}

function submitFormAjaxAndDoAction( path, formId, doAction, quiet = false  ){
    var isSuccess = false;
    if(formId != null ){
        if( formId[0] != '#' ){
            formId  = '#'+formId;
        }
        var formData        = new FormData ( $(formId)[0] );
    }else{
        formData    = "";
    }
    $.ajax({
        url: path,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success:function (data) {
            if( doAction != null){
                if (doAction.length > 0) {
                    doAction(data);
                }else {
                    doAction();
                }
            }
            return false;
        },
        error:function (data) {
            if(!quiet){
                if( data.responseJSON.message != undefined ){
                    swal("Erreur :(", data.responseJSON.message, "error");
                }else{
                    swal("Erreur :(", "Une erreur est survenue lors de l'enregistrement. Contactez un administrateur.", "error");
                }
            }
            return false;
        },
        complete:function () {
            $(".ajax-loader").remove();
        },

    });
    return isSuccess;
}

function openModal(modalId, path, callback){

    //display loading modal
    $('#'+modalId).modal('show');
    if(path){
        $.ajax({
            url: path,
            type: 'GET',
            data: {},
            success: function(response){
                // Add response in Modal body
                $modalContainer = $('#'+modalId+' .modal-content');
                $modalContainer.html(response);
                $('#'+modalId+' select').select2();
                if( callback != null){
                    callback();
                }
            }
        });
    }
}