$(document).ready( function(){
    addEventOnTemplateButtons();
    $("#modal").on("elementChanged", function (){
        reloadBlogContent();
        $("#modal").modal('hide')
    })
    initBlogEdition();
});

function initBlogEdition(){
    $(".blog-main-render>div").addClass("element-edit");
    addEditButtons();
    $(".blog-main-render").sortable({
        stop: function (){
            stringifySortable();
        }
    });
    $("#btn-rename").click(renameBlog);
}

function reloadBlogContent(){
    var url = $("#url-get-blogrender").val();
    getJsonFromController(url, function (data){
        $("#blog-renderview").html(data.render);
        initBlogEdition();
    });
}

function addEditButtons(){
    var elements = $(".blog-main-render>div");
    elements.each( i => {
        $(elements[i]).prepend(`
        <div class="row content-element-tools">
            <div class="col-sm-12 text-right">
<!--                <button class="btn btn-success" data-toggle="tooltip" title="Insérer du contenu avant ce bloc"><i class="fas fa-level-up-alt"></i></button>-->
                <button class="btn btn-label btn-label-primary btn-bold" data-toggle="tooltip" onclick="updateContentElement(this)" title="Modifier le contenu"><i class="fas fa-pen"></i></button>
                <button class="btn btn-danger" data-toggle="tooltip" title="Supprimer le contenu" onclick="deleteElement(this)"><i class="fas fa-trash"></i></button>
            </div>
        </div>
        `)
    })
}

function stringifySortable(){
    var elements    = $(".blog-main-render>div");
    var json        = [];
    var url         = $("#url-sort-element").val();
    elements.each( i => {
        json.push({
            "type":$(elements[i]).data('class'),
            "id":$(elements[i]).data('id'),
        })
    })

    $.ajax({
        type: "POST",
        data: JSON.stringify(json),
        dataType: 'json',
        url: url,
        cache: false,
        success: function(data){
          console.log(data);
        },
        error: function(data){
            swal("Erreur :(", "Une erreur est survenue. Veuillez contacter un administrateur", "error");
        }
    })

}



function addContentMenu(){
    $(".add-content").hide('fast');
    $(".select-content").show('fast')
}
function cancelAddMenu(){
    $(".add-content").show();
    $(".select-content").hide()
}

function editMenuCard(){
    $("#contextual-menu").hide('fast');
    $(".add-content").hide('fast');
    $("#add-element-template").show('fast')
}

function updateMenuCard(){
    $("#contextual-menu").hide('fast');
    $(".add-content").hide('fast');
    $("#update-element-template").show('fast')
}

function cancelInsertContentElement(){
    $("#contextual-menu").show('fast');
    cancelAddMenu();
    $("#add-element-template").hide();
    $("#update-element-template").hide();
    $('#add-element-template').empty();
    $('#update-element-template').empty();
}

function resizeElement( element ){
    var editBloc    = $(element).closest('.element-edit');
    var type        = $(editBloc[0]).data('class');
    var id          = $(editBloc[0]).data('id');
    var url         = $("#url-resize-element").val();
    url = url.replace('ID', id);
    url = url.replace('TYPE', type);
    openModal('modal', url)
}

function deleteElement( element ){
    swal.fire({
        title: "Supprimer le bloc",
        text: "Le contenu sera effacé et ne sera plus récupérable.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#F44336",
        confirmButtonText: "Effacer",
        cancelButtonText: "Annuler",
        closeOnConfirm: true
    }).then(function (res){
        if (res.value) {
            var editBloc    = $(element).closest('.element-edit');
            var type        = $(editBloc[0]).data('class');
            var id          = $(editBloc[0]).data('id');
            var url         = $("#url-delete-element").val();
            url = url.replace('ID', id);
            url = url.replace('TYPE', type);

            getJsonFromController( url, function () {
                reloadBlogContent();
            })
        }
    });
}

function addEventOnTemplateButtons(){
    var buttons = $("#add-templates-buttons button");
    buttons.each( function(){
        var type = $(this).data('class');
        $(this).click( () => {
            editMenuCard();
            loadAddElementTemplate( type );
        })
    })
}

function loadAddElementTemplate( type ){
    var url = $('#add-element-template').data('path');
    url = url.replace('_TYPE_', type);
    $('#add-element-template').load(url);
}

function renameBlog(){
    var url = $("#url-rename-blog").val();
    openModal('modal', url)
}

function submitRename(){
    postFormAjax( "#form-update-blog", data => {
        if( data.id != undefined ){
            reloadBlogContent();
            $("#modal").modal('hide');
        }
    } )
}

function updateContentElement( element ){
    var editBloc    = $(element).closest('.element-edit');
    var type        = $(editBloc[0]).data('class');
    var id          = $(editBloc[0]).data('id');
    loadUpdateBloc( id, type);
    updateMenuCard();
}

function loadUpdateBloc(id, type){
    var url = $('#update-element-template').data('path');
    url = url.replace('_TYPE_', type);
    url = url.replace('_ID_', id);
    $('#update-element-template').load(url, () => {
        window.scrollTo(0,document.body.scrollHeight);
    });
}

function getJsonFromController(path, callback){
    submitFormAjaxAndDoAction(path, null, callback);
}

function submitFormAjaxAndDoAction( path, formId, doAction, quiet = false  ){
    var isSuccess = false;
    if(formId != null ){
        if( formId[0] != '#' ){
            formId  = '#'+formId;
        }
        var formData        = new FormData ( $(formId)[0] );
    }else{
        formData    = "";
    }
    $.ajax({
        url: path,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success:function (data) {
            if( doAction != null){
                if (doAction.length > 0) {
                    doAction(data);
                }else {
                    doAction();
                }
            }
            return false;
        },
        error:function (data) {
            if(!quiet){
                if( data.responseJSON.message != undefined ){
                    swal("Erreur :(", data.responseJSON.message, "error");
                }else{
                    swal("Erreur :(", "Une erreur est survenue lors de l'enregistrement. Contactez un administrateur.", "error");
                }
            }
            return false;
        },
        complete:function () {
            $(".ajax-loader").remove();
        },

    });
    return isSuccess;
}

function openModal(modalId, path, callback){

    //display loading modal
    $('#'+modalId).modal('show');
    if(path){
        $.ajax({
            url: path,
            type: 'GET',
            data: {},
            success: function(response){
                // Add response in Modal body
                $modalContainer = $('#'+modalId+' .modal-content');
                $modalContainer.html(response);
                $('#'+modalId+' select').select2();
                if( callback != null){
                    callback();
                }
            }
        });
    }
}

function  postFormAjax(formId, successCallback) {
    $(formId).append(`
    <div class="row ajax-loader m-t-20">
        <div class="col-sm-12 text-center">
           <!-- mettre spinner -->
        </div>
    </div>
    `)
    return submitFormAjaxAndDoAction($(formId)[0].action, formId, successCallback);
}