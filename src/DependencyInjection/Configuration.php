<?php


namespace Kalitics\ManualBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        if (method_exists(TreeBuilder::class, 'getRootNode')) {
            $treeBuilder = new TreeBuilder('kalitics_manual');
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $treeBuilder = new TreeBuilder();
            $rootNode = $treeBuilder->root('kalitics_manual');
        }

        $rootNode
            ->children()
            ->scalarNode('path')->defaultValue("/")->info('Path of Manual folder')
            ->end()
        ;

        return $treeBuilder;
    }
}
