<?php

namespace Kalitics\ManualBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ManualControllerTest extends WebTestCase
{
    public function testList(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/list');
    }

    public function testAdd(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/add');
    }

    public function testEdit(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/edit');
    }

    public function testDelete(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/delete');
    }

    public function testShow(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/show');
    }

}
