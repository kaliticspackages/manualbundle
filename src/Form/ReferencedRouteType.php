<?php

namespace Kalitics\ManualBundle\Form;

use Kalitics\ManualBundle\Entity\UserDocumentation\Blog;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Kalitics\ManualBundle\Entity\ReferencedRoute;

class ReferencedRouteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder    ->add('blog', EntityType::class, [
            "label"     => "Page de documentation",
            "class"     => Blog::class
        ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ReferencedRoute::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'manualbundle_referencedroute';
    }


}
