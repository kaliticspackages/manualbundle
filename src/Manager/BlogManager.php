<?php

namespace Kalitics\ManualBundle\Manager;

use Kalitics\ManualBundle\Entity\ReferencedRoute;
use Kalitics\ManualBundle\Entity\UserDocumentation\Blog;
use Kalitics\ManualBundle\Entity\UserDocumentation\ContentElement\CkeditorElement;
use Kalitics\ManualBundle\Entity\UserDocumentation\ContentElement\ImageElement;
use Kalitics\ManualBundle\Entity\UserDocumentation\IContentElement;
use Kalitics\ManualBundle\Repository\UserDocumentation\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class BlogManager {

    const CONTENT_ELEMENTS = [
        CkeditorElement::class => [
            'icon'      => '<i class="fas fa-font"></i>',
            'label'     => 'Zone de texte',
        ],
        ImageElement::class => [
            'icon'      => '<i class="fas fa-image"></i>',
            'label'     => "Zone d'image",
        ]
    ];

    const UPLOAD_FILE_PATH = "/uploads/user_documentation";

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /** @var BlogRepository */
    private $blogRepository;

    /**
     * @var Blog
     */
    private $blog = null;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BlogManager constructor.
     * @param EntityManagerInterface $manager
     * @param ContainerInterface $container
     */
    public function __construct(
        EntityManagerInterface $manager,
        ContainerInterface $container
    )
    {
        $this->manager = $manager;
        $this->blogRepository = $manager->getRepository(Blog::class);
        $this->container = $container;
    }

    /**
     * @param string|null $id
     * @return Blog|null
     */
    public function getBlog(?string $id = null): ?Blog
    {
        if( $this->blog !== null ){
            return $this->blog;
        }
        $this->blog = $this->blogRepository->find($id);
        return $this->blog;
    }

    /**
     * @return Blog[]
     */
    public function findAll(): array
    {
        return $this->blogRepository->findAll();
    }


    /**
     * @param Blog $blog
     * @return IContentElement[]
     */
    public function getBlogElements( Blog $blog ):array
    {
        $elements = [];
        foreach ( self::CONTENT_ELEMENTS as $class => $attributes ){
            $elements = array_merge( $elements, $this->manager->getRepository($class)->findBy(["blog" => $blog]) );
        }
        return $elements;
    }

    /**
     * @param Blog $blog
     * @return IContentElement[]
     */
    public function getSortedBlogElements( Blog $blog ):array
    {
        return $this->sortBlogElements( $this->getBlogElements($blog) );
    }

    /**
     * @param array $blogElements
     * @return IContentElement[]
     */
    public function sortBlogElements( array $blogElements ):array
    {
        $sorted = [];
        /** @var IContentElement $element */
        foreach ($blogElements as $element) {
            $sorted[$element->getPosition()] = $element;
        }
        if( ksort($sorted) ){
            return $sorted;
        }
        return $blogElements;
    }

    /**
     * @param string $type
     * @return string
     */
    public function getClassFromType( string $type ): ?string
    {
        foreach ( self::CONTENT_ELEMENTS as $class => $attributes ){
            $typeClass = explode('\\', $class);
            if( end($typeClass) === $type ){
                return $class;
            }
        }

        return null;
    }

    /**
     * @param string $class
     * @return string|null
     */
    public function getTypeFromClass( string $class ): string
    {
        $typeClass = explode("\\", $class);
        return end( $typeClass );
    }

    /**
     * @param Blog $blog
     * @param string $type
     * @param Request $request
     * @return IContentElement
     */
    public function createContentFromRequestData(Blog $blog, string $type, Request $request): IContentElement
    {
        $data = $this->normalizeRequestForContent($request);
        $class = $this->getClassFromType($type);

        /** @var IContentElement $contentElement */
        $contentElement = new $class;
        $contentElement->setContent($data);
        $contentElement->setBlog($blog);
        $contentElement->setPosition( $this->countContentElementsForBlog($blog) );
        $this->manager->persist($blog);
        $this->manager->persist($contentElement);
        $this->manager->flush();
        return $contentElement;
    }

    /**
     * @param IContentElement $contentElement
     * @param string $type
     * @param Request $request
     * @return IContentElement
     */
    public function updateContentFromRequestData(IContentElement $contentElement, string $type, Request $request): IContentElement
    {
        $data  = $this->normalizeRequestForContent($request);
        $class = $this->getClassFromType($type);
        $blog  = $contentElement->getBlog();
        /** @var IContentElement $contentElement */
        $clonedContent = clone $contentElement;
        $clonedContent->setContent($data);
        $this->manager->remove($contentElement);
        $this->manager->persist($blog);
        $this->manager->persist($clonedContent);
        $this->manager->flush();
        return $clonedContent;
    }

    /**
     * @param Blog $blog
     * @return \Closure|int
     */
    public function countContentElementsForBlog( Blog $blog )
    {
        $count = 0;
        foreach ( self::CONTENT_ELEMENTS as $class => $attr ){
            /** @var EntityRepository $repo */
            $repo = $this->manager->getRepository($class);
            $qb = $repo->createQueryBuilder('e')
                ->select('count(e)')
                ->andWhere('e.deletedAt is null')
                ->andWhere('e.blog = :blog')
                ->setParameter('blog', $blog);
            try {
                $count += $qb->getQuery()->getSingleScalarResult();
            } catch (NoResultException | NonUniqueResultException $e) {
                continue;
            }
        }
        return $count;
    }

    /**
     * @param string $path
     * @return Blog|null
     */
    public function getBlogFromPath( string $path ): ?Blog
    {
        $route = $this->getReferencedRouteFromPath( $path );
        return $route->getBlog();
    }

    /**
     * @param string $path
     * @return ReferencedRoute|null
     */
    public function getReferencedRouteFromPath(string $path ): ?ReferencedRoute
    {
        $route = $this->manager->getRepository(ReferencedRoute::class)->findOneBy(["route" => $path]);
        if( null === $route ){
            return new ReferencedRoute( $path );
        }
        return $route;
    }

    /**
     * @param $id
     * @return ReferencedRoute|null
     */
    public function getReferencedRoute( $id ): ?ReferencedRoute
    {
        return  $this->manager->getRepository(ReferencedRoute::class)->find($id);
    }

    /**
     * @param string $path
     * @return Blog
     */
    public function createBlog( string $path ): Blog
    {
        $blog = new Blog();
        $blog->setTitle("Ajouter un titre pour cette page de documentation");
        $blog->addReferencedRoutes( $this->getReferencedRouteFromPath($path) );
        return $blog;
    }

    /**
     * @param Blog $blog
     * @return $this
     */
    public function save( Blog $blog ): BlogManager
    {
        $this->manager->persist($blog);
        $this->manager->flush();
        return $this;
    }

    /**
     * @param ReferencedRoute $route
     * @return $this
     */
    public function saveReferencedRoute( ReferencedRoute $route ): BlogManager
    {
        $this->manager->persist($route);
        $this->manager->flush();
        return $this;
    }


    /**
     * @param Request $request
     * @return array
     */
    public function normalizeRequestForContent( Request $request ): array
    {
        $normalised = [
            "values"    => [],
            "files"     => []
        ];
        foreach ( $request->request->all() as $key => $val ){
            $normalised['values'][$key] = $val;
        }
        $files = $request->files->all();
        foreach ( $files as $key => $file ){
            $normalised['files'][$key]['path']      = self::UPLOAD_FILE_PATH;
            $normalised['files'][$key]['filename']  = $this->upload($file, "/user_documentation");
        }

        return $normalised;
    }

    /**
     * @param string $type
     * @param string $id
     * @return IContentElement|null
     */
    public function getContentElement( string $type, string $id): ?IContentElement
    {
        if( null === ( $class = $this->getClassFromType($type) )){
            return null;
        }
        /** @var IContentElement|null $content */
        $content = $this->manager->getRepository($class)->find($id);
        return $content;
    }

    /**
     * @param Blog $blog
     * @param array $requestData
     * @return bool
     */
    public function sortElementsFromRequestData( Blog $blog, array $requestData ): bool
    {
        foreach ( $requestData as $key => $data ){
            $element = $this->getContentElement( $data['type'], $data['id'] );
            if( $element !== null ){
                $element->setPosition($key);
                $this->manager->persist($element);
            }
        }
        $this->manager->flush();
        return true;
    }

    /**
     * @param IContentElement $element
     * @param int $width
     * @return int
     */
    public function resizeElement( IContentElement $element, int $width ): int
    {
        $element->setWidth($width);
        $this->manager->persist($element);
        $this->manager->flush();
        return $element->getWidth();
    }

    /**
     * @param IContentElement $contentElement
     * @return bool
     */
    public function deleteElement( IContentElement $contentElement ): bool
    {
        $blog = $contentElement->getBlog();
        $this->manager->remove($contentElement);
        $this->manager->flush();
        $this->resetSortPosition( $blog );
        return true;
    }

    /**
     * @param Blog $blog
     */
    public function resetSortPosition( Blog $blog ): void
    {
        $elements = $this->getBlogElements($blog);
        $sortedElements = $this->sortBlogElements( $elements );
        $newIndex = 0;
        foreach ( $sortedElements as $el ){
            $el->setPosition($newIndex);
            $this->manager->persist($el);
            $newIndex ++;
        }
        $this->manager->flush();
    }

    /**
     * @param Blog $blog
     */
    public function deleteBlog( Blog $blog ): void
    {
        $this->manager->remove($blog);
        $this->manager->flush();
    }

    /**
     * @param ReferencedRoute $referencedRoute
     */
    public function deleteReferencedRoute( ReferencedRoute $referencedRoute ): void
    {
        $this->manager->remove($referencedRoute);
        $this->manager->flush();
    }

    // ==== Add upload and getTargetDirectory function to not depend on FileUploader service ====
    public function upload(UploadedFile $file, $destination = "", $filePrefix = ""): string
    {
        $extension = $file->guessExtension();
        if (!$extension) {
            $extension = $file->getClientOriginalExtension();
        }
        $fileName = $filePrefix.md5(uniqid('', true)).'.'.$extension;
        if(!file_exists($this->getTargetDirectory() . '/' . $destination) &&
            !mkdir($concurrentDirectory = $this->getTargetDirectory() . "/" . $destination) && !is_dir($concurrentDirectory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        $file->move($this->getTargetDirectory().'/'.$destination, $fileName);
        return $fileName;
    }

    public function getTargetDirectory(){
        return $this->container->getParameter('upload_directory');
    }
}