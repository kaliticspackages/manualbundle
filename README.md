# Configuration

Add in config/routes/kalitics_manual.yaml
```yaml
_kalitics_manual:
  resource: '@KaliticsManualBundle/Resources/config/routes.xml'
  prefix: /kalitics/manual/
```

Add in config/packages/twig.yaml
```yaml
'%kernel.project_dir%/vendor/kalitics/manual-bundle/src/Resources/views/': KaliticsManual
```

Add to config/bundle.php
```php
Kalitics\ManualBundle\KaliticsManualBundle::class => ['all' => true]
```

config/services.yaml
```yaml
 Kalitics\ManualBundle\Controller\UserDocumentationController:
        class: Kalitics\ManualBundle\Controller\UserDocumentationController
        arguments: ['@kalitics_manual.blog_manager', '@kalitics_manual.blog_render_service', '@router']
```

******

## Ckeditor

```bash
composer require friendsofsymfony/ckeditor-bundle 
php bin/console ckeditor:install
php bin/console assets:install public
```

***config/packages/fos_ckeditor.yaml***

Add in form_themes :
```yaml
twig:
    form_themes:
        - '@FOSCKEditor/Form/ckeditor_widget.html.twig'
```

